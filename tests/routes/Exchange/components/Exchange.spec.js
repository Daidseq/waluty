import React from 'react'
import { bindActionCreators } from 'redux'
import { Exchange } from 'routes/Exchange/components/Exchange'
import { shallow } from 'enzyme'

describe('(Component) Exchange', () => {

  describe('Should render ask/bid currency...', () => {
    let _props, _spies, _wrapper
    beforeEach(() => {
      _spies = {}
      _props = {
        ask: 4.0123,
        bid: 4.0152,
        name: 'USD',
        ...bindActionCreators({
          delete: (_spies.delete = sinon.spy())
        }, _spies.dispatch = sinon.spy())
      }
      _wrapper = shallow(<Exchange {..._props} />)
    })

    it('Should render as a <div>.', () => {
      expect(_wrapper.is('div')).to.equal(true)
    })

    it('Should render with h3 that includes "USD" text.', () => {
      expect(_wrapper.find('h3').text()).to.match(/USD/)
    })

    it('Should render two divs ask/bid.', () => {
      expect(_wrapper.contains(
        <div className="ask">Ask: {4.0123}</div>
      )).to.be.true
      expect(_wrapper.contains(
        <div className="bid">Bid: {4.0152}</div>
      )).to.be.true
      expect(_wrapper.contains(
        <div className="mid">Mid: {}</div>
      )).to.be.false
    })

    describe('A Double (Async) button...', () => {
      let _button

      beforeEach(() => {
        _button = _wrapper.find('button')
      })

      it('has bootstrap classes', () => {
        expect(_button.hasClass('btn btn-default')).to.be.true
      })

      it('Should dispatch a `delete` action when clicked', () => {
        _spies.dispatch.should.have.not.been.called

        _button.simulate('click')

        _spies.dispatch.should.have.been.called
        _spies.delete.should.have.been.called
      })
    })
  })
})
