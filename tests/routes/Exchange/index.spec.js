import ExchangeRoute from 'routes/Exchange'

describe('(Route) Exchange', () => {
  let _route

  beforeEach(() => {
    _route = ExchangeRoute({})
  })

  it('Should return a route configuration object', () => {
    expect(typeof _route).to.equal('object')
  })

  it('Configuration should contain path `exchange`', () => {
    expect(_route.path).to.equal('exchange')
  })
})
