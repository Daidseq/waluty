import {
  EXCHANGE_GET_TABLES_SUCCESS,
  EXCHANGE_GET_TABLES_REQUEST,
  EXCHANGE_CHANGE_OBSERVED_CURRENCIES,
  getTables,
  getTablesRequest,
  getTablesSuccess,
  addObservedCurrency,
  deleteObservedCurrency,
  changeObservedCurrencies,
  initialState,
  default as exchangeReducer
} from 'routes/Exchange/modules/exchange'


describe('(Redux Module) Exchange', () => {
  it('Should export a constant EXCHANGE_GET_TABLES_SUCCESS.', () => {
    expect(EXCHANGE_GET_TABLES_SUCCESS).to.equal('EXCHANGE_GET_TABLES_SUCCESS')
  })

  it('Should export a constant EXCHANGE_GET_TABLES_REQUEST.', () => {
    expect(EXCHANGE_GET_TABLES_REQUEST).to.equal('EXCHANGE_GET_TABLES_REQUEST')
  })

  it('Should export a constant EXCHANGE_CHANGE_OBSERVED_CURRENCIES.', () => {
    expect(EXCHANGE_CHANGE_OBSERVED_CURRENCIES).to.equal('EXCHANGE_CHANGE_OBSERVED_CURRENCIES')
  })

  describe('(Reducer)', () => {
    it('Should be a function.', () => {
      expect(exchangeReducer).to.be.a('function')
    })

    it('Should initialize with a initial object', () => {
      expect(exchangeReducer(undefined, {})).to.eql(initialState);
    })

    it('Should return the previous state if an action was not matched.', () => {
      let state = exchangeReducer(undefined, {})

      expect(state).to.eql(initialState)
      state = exchangeReducer(state, { type: '@@@@@@@' })
      expect(state).to.eql(initialState)
      state = exchangeReducer(state, getTablesRequest())
      expect(state).to.eql({
        ...initialState,
        loading: true
      })
      state = exchangeReducer(state, { type: '@@@@@@@' })
      expect(state).to.eql({
        ...initialState,
        loading: true
      })
    })

    describe('(Action Creator) getTablesRequest', () => {
      it('Should be exported as a function.', () => {
        expect(getTablesRequest).to.be.a('function')
      })

      it('Should return an action with type "EXCHANGE_GET_TABLES_REQUEST".', () => {
        expect(getTablesRequest()).to.have.property('type', EXCHANGE_GET_TABLES_REQUEST)
      })

      it('Should change loading in state to true.', () => {
        let state = exchangeReducer(undefined, {})
        state = exchangeReducer(state, getTablesRequest())
        expect(state).to.eql({
          ...initialState,
          loading: true
        })
      })
    })
  })
})
