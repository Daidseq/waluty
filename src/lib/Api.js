'use strict';

// import _ from 'lodash';

// Singleton instance
let instance = null;

class Api {  
  constructor() {
    if(!instance){
      instance = this;
    }

    return instance;
  }

  async getExchangeRateTable(tableType) {
    return await fetch('http://api.nbp.pl/api/exchangerates/tables/' + tableType + '?format=json')
      .then((response) => {
        let json = response.json();
        if (response.status === 200) {
          return json;
        } else {
          return json.then((json) => {
            throw 'Bad request';
          });
        }
      })
      .catch((error) => {
        throw error;
      });
  }
}

export default Api;