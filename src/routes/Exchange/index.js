import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path : 'exchange',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Exchange = require('./containers/ExchangeContainer').default
      const reducer = require('./modules/exchange').default

      /*  Add the reducer to the store on key 'exchange'  */
      injectReducer(store, { key: 'exchange', reducer })

      /*  Return getComponent   */
      cb(null, Exchange)

    /* Webpack named bundle   */
    }, 'exchange')
  }
})
