import React from 'react';

import { connect } from 'react-redux'
import { getTables, addObservedCurrency, deleteObservedCurrency } from '../modules/exchange'
import Exchange from '../components/Exchange';

import Select from 'react-select';
import 'react-select/dist/react-select.css';

const REFRESH_EVERY = 5000;

const mapDispatchToProps = {
  getTables,
  addObservedCurrency,
  deleteObservedCurrency
}

const mapStateToProps = (state) => ({
  currencies: state.exchange.currencies,
  loading: state.exchange.loading,
  observedCurrencies: state.exchange.observedCurrencies
})

class ExchangeContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillReceiveProps(props) {
    let allCurrencyOptions = Object.keys(props.currencies).map((key) => ({
      value: props.currencies[key].code,
      label: props.currencies[key].code + ' (' + props.currencies[key].currency + ')'
    }));

    this.setState({options: allCurrencyOptions});
  }

  componentDidMount() {
    this.props.getTables();
    this.state.refresh = setInterval(() => {
      this.props.getTables();
    }, REFRESH_EVERY)
  }

  componentWillUnmount() {
    clearInterval(this.state.refresh);
  }

  addObservedCurrency(currencyOption) {
    this.props.addObservedCurrency(currencyOption.value);
  }

  renderObservedCurrencies(observedCurrencies, currencies) {
    return observedCurrencies.map((currencyName, index) => {
      let currencyObject = currencies[currencyName];
      return (
        <Exchange
          key={index}
          name={currencyObject.code}
          ask={currencyObject.ask}
          bid={currencyObject.bid}
          mid={currencyObject.mid}
          delete={() => this.props.deleteObservedCurrency(currencyObject.code)}
        />
      );
    });
  }

  render () {
    return (
      <div>
        {
          this.props.loading ? (
            <div>loading...</div>
          ):(
            <Select
              name="select-currency"
              options={this.state.options}
              onChange={this.addObservedCurrency.bind(this)}
            />
          )
        }
        {
          this.renderObservedCurrencies(this.props.observedCurrencies, this.props.currencies)
        }
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExchangeContainer)
