import React from 'react'

import './Exchange.scss';

export const Exchange = (props) => (
  <div className="currency">
    <h3>
      {props.name}
    </h3>
    {
      props.ask && <div className="ask">Ask: {props.ask}</div>
    }
    {
      props.bid && <div className="bid">Bid: {props.bid}</div>
    }
    {
      props.mid && <div className="mid">Mid: {props.mid}</div>
    }
    <button className='btn btn-default' onClick={props.delete}>Delete</button>
  </div>
)

Exchange.propTypes = {
  name: React.PropTypes.string,
  ask: React.PropTypes.number,
  bid: React.PropTypes.number,
  mid: React.PropTypes.number,
  delete: React.PropTypes.func
}

export default Exchange
