import _ from 'lodash';

import Api from '../../../lib/Api';

// ------------------------------------
// Constants
// ------------------------------------
export const EXCHANGE_GET_TABLES_SUCCESS = 'EXCHANGE_GET_TABLES_SUCCESS'
export const EXCHANGE_GET_TABLES_REQUEST = 'EXCHANGE_GET_TABLES_REQUEST'
export const EXCHANGE_CHANGE_OBSERVED_CURRENCIES = 'EXCHANGE_CHANGE_OBSERVED_CURRENCIES'

// ------------------------------------
// Actions
// ------------------------------------
export const getTables = () => {
  return (dispatch, getState) => {
    if (!getState().exchange.afterFirstLoad) {
      dispatch(getTablesRequest());
    }
    let api = new Api();

    return api.getExchangeRateTable('A').then((responseA) => {
      return api.getExchangeRateTable('B').then((responseB) => {
        return api.getExchangeRateTable('C').then((responseC) => {
          dispatch(getTablesSuccess(responseA, responseB, responseC))
        })
        .catch((error) => {
          throw error;
        });
      })
      .catch((error) => {
        throw error;
      });
    })
    .catch((error) => {
      // TODO: HANDLE API ERRORS
    });
  }
}

export const getTablesRequest = () => {
  return {
    type: EXCHANGE_GET_TABLES_REQUEST
  }
}

export const getTablesSuccess = (tableA, tableB, tableC) => {
  let midCurrencies = tableA[0].rates.concat(tableB[0].rates);
  let buySellCurrencies = tableC[0].rates;
  let concatenatedCurrencies = buySellCurrencies.concat(midCurrencies);
  let distinctCurrencies = {};

  for (let index in concatenatedCurrencies) {
    let currency = concatenatedCurrencies[index];
    if (!distinctCurrencies[currency.code]) {
      distinctCurrencies[currency.code] = currency;
    }
  }
  
  return {
    type: EXCHANGE_GET_TABLES_SUCCESS,
    payload: { distinctCurrencies }
  }
}

export const addObservedCurrency = (currencyCode) => {
  return (dispatch, getState) => {
    let observedCurrencies = _.clone(getState().exchange.observedCurrencies);
    
    if (observedCurrencies.indexOf(currencyCode) === -1) {
      observedCurrencies.push(currencyCode);
    }
    dispatch(changeObservedCurrencies(observedCurrencies));
  }
}

export const deleteObservedCurrency = (currencyCode) => {
  return (dispatch, getState) => {
    let observedCurrencies = _.clone(getState().exchange.observedCurrencies);
    let index = observedCurrencies.indexOf(currencyCode);

    if (index !== -1) {
      observedCurrencies.splice(index, 1);
    }
    dispatch(changeObservedCurrencies(observedCurrencies));
  }
}

export const changeObservedCurrencies = (observedCurrencies) => {
  return {
    type: EXCHANGE_CHANGE_OBSERVED_CURRENCIES,
    payload: { observedCurrencies }
  }
}

export const actions = {
  getTables,
  getTablesSuccess,
  getTablesRequest,
  changeObservedCurrencies,
  deleteObservedCurrency,
  addObservedCurrency
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [EXCHANGE_GET_TABLES_REQUEST]: (state, action) => (
    {
      ...state,
      loading: true
    }
  ),
  [EXCHANGE_GET_TABLES_SUCCESS]: (state, action) => (
    {
      ...state,
      currencies: action.payload.distinctCurrencies,
      loading: false,
      afterFirstLoad: true
    }
  ),
  [EXCHANGE_CHANGE_OBSERVED_CURRENCIES]: (state, action) => (
    {
      ...state,
      observedCurrencies: action.payload.observedCurrencies
    }
  )
}

// ------------------------------------
// Reducer
// ------------------------------------
export const initialState = {
  currencies: {},
  observedCurrencies: [],
  loading: false,
  afterFirstLoad: false
}

export default function exchangeReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
